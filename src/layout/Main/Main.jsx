import React from "react";
import PromptRender from "../../components/PromptRender/PromptRender";
import PromptParams from "../../components/PromptParams/PromptParams";
import { FormProvider } from "../../context/FormContext";

const Main = () => {
  return (
    <FormProvider>
      <main className="container p-10 px-4 md:py-[90px] mx-auto lg:flex">
        <div className="lg:w-1/2">
          <PromptParams />
        </div>
        <div className="lg:w-1/2 mt-4 lg:mt-0 lg:ml-4">
          <PromptRender />
        </div>
      </main>
    </FormProvider>
  );
};

export default Main;
