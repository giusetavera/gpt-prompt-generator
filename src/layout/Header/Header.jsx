import React from "react";
import Title from "../../common/Title";
import logo from "../../img/logo.svg";

const Header = () => {
  return (
    <header className="shadow-sm py-4 px-4 flex justify-center">
      <div className="flex justify-between items-center">
        <img src={logo} alt="Logo" className="max-w-[50px] mr-3" />
        <div className="header__title">
          <Title text="Prompt Generator" className="" />
          <p className="text-slate-500">
            powered by{" "}
            <strong>
              <a
                className="underline"
                href="https://giuseppetavera.it"
                target="_blank"
                rel="noreferrer"
              >
                Giuseppe Tavera
              </a>
            </strong>
          </p>
        </div>
      </div>
    </header>
  );
};

export default Header;
