import React from "react";

const Footer = () => {
  return (
    <footer className="w-full text-gray-700 bg-slate-200 body-font">
      <div className="container px-4 py-6 mx-auto">
        <p className="text-sm text-gray-700 capitalize text-center">
          © 2024 | <strong>Giuseppe Tavera</strong> | All rights reserved
        </p>
      </div>
    </footer>
  );
};

export default Footer;
