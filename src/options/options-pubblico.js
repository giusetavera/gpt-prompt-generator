export const optPubblicoDestinazione = [
  {
    value: "appassionati-tecnologia",
    label: "Appassionati di tecnologia",
  },
  {
    value: "clienti-azienda",
    label: "Clienti di un'azienda",
  },
  {
    value: "appassionati-cucina",
    label: "Appassionati di cucina",
  },
  {
    value: "sportivi",
    label: "Sportivi",
  },
];
