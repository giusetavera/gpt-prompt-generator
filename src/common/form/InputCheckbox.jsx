import React from "react";

const InputCheckbox = ({
  id,
  name,
  checked,
  onChange,
  required,
  className,
}) => {
  const classes = `form-checkbox h-4 w-4 text-blue-600 transition duration-150 ease-in-out ${className}`;

  return (
    <input
      id={id}
      name={name}
      type="checkbox"
      checked={checked}
      onChange={onChange}
      required={required}
      className={classes}
    />
  );
};

export default InputCheckbox;
