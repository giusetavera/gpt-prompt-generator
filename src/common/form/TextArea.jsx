import React from "react";

const TextArea = ({ id, name, value, onChange, className, placeholder }) => {
  const classes = `block w-full mt-1 p-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:border-blue-500 focus:ring focus:ring-blue-500 focus:ring-opacity-50 ${className}`;

  return (
    <textarea
      id={id}
      name={name}
      value={value}
      onChange={onChange}
      className={classes}
      placeholder={placeholder}
      rows="4"
    ></textarea>
  );
};

export default TextArea;
