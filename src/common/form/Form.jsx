import React from "react";

const Form = ({ id, method, onSubmit, onReset, className, children }) => {
  const classes = `block w-full mx-auto ${className}`;

  return (
    <form
      method={method}
      id={id}
      onSubmit={onSubmit}
      onReset={onReset}
      className={classes}
    >
      {children}
    </form>
  );
};

export default Form;
