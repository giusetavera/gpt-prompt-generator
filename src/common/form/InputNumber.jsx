import React from "react";

const InputNumber = ({
  id,
  name,
  value,
  onChange,
  placeholder,
  min,
  max,
  step,
}) => {
  return (
    <input
      type="number"
      id={id}
      name={name}
      value={value}
      onChange={onChange}
      placeholder={placeholder}
      min={min}
      max={max}
      step={step}
      className="block w-full mt-1 p-2 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:border-blue-500 focus:ring focus:ring-blue-500 focus:ring-opacity-50"
    />
  );
};

export default InputNumber;
