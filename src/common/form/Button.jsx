import React from "react";

const Button = ({ type, onClick, className, children, disabled }) => {
  let classes = `px-4 py-2 bg-blue-500 border border-transparent rounded-md font-semibold text-white uppercase tracking-widest hover:bg-blue-600 active:bg-blue-700 focus:outline-none focus:border-blue-700 focus:ring focus:ring-blue-500 disabled:opacity-50 ${className}`;

  if (type === "reset") {
    classes = `px-4 py-2 bg-white border border-2 rounded-md font-semibold text-blue-500 uppercase tracking-widest hover:text-blue-700 active:text-blue-700 focus:outline-none focus:border-blue-700 disabled:opacity-50 ${className}`;
  }

  return (
    <button
      type={type}
      className={classes}
      onClick={onClick}
      disabled={disabled}
    >
      {children}
    </button>
  );
};

export default Button;
