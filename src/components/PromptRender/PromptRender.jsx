import React, { useContext } from "react";
import Title from "../../common/Title";
import Button from "../../common/form/Button";
import { promptRender } from "../../functions/promptRender";
import { copyToClipboard } from "../../functions/copyToClipboard";

// Context
import { FormContext } from "../../context/FormContext";

const PromptRender = () => {
  // Get values from context (form)
  const { formValues } = useContext(FormContext);

  // Use form values
  const prompt = promptRender(formValues);

  console.log(prompt);

  return (
    <div className="prompt-render">
      <div className="flex items-center justify-between mb-4">
        <Title text="Il tuo prompt" className="mb-0" />
        <Button className="md:ml-2" onClick={copyToClipboard}>
          Copia
        </Button>
      </div>

      <div className="prompt-render__content min-h-[500px] p-4 md:p-6 border-blue-500 rounded-md w-full h-full bg-slate-100">
        {formValues.titoloArticolo && (
          <div dangerouslySetInnerHTML={{ __html: prompt }}></div>
        )}
      </div>
    </div>
  );
};

export default PromptRender;
